package test;

import java.util.Random;

import controller.BunteRechteckeController;
import model.Punkt;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {

		//Parameterlose Rechtecke
		Rechteck r0 = new Rechteck();
		r0.setPunkt(new Punkt(10, 10));
		r0.setBreite(30);
		r0.setHoehe(40);
		Rechteck r1 = new Rechteck();
		r1.setPunkt(new Punkt(25, 25));
		r1.setBreite(100);
		r1.setHoehe(20);
		Rechteck r2 = new Rechteck();
		r2.setPunkt(new Punkt(260, 10));
		r2.setBreite(200);
		r2.setHoehe(100);
		Rechteck r3 = new Rechteck();
		r3.setPunkt(new Punkt(5, 500));
		r3.setBreite(300);
		r3.setHoehe(25);
		Rechteck r4 = new Rechteck();
		r4.setPunkt(new Punkt(100, 100));
		r4.setBreite(100);
		r4.setHoehe(100);
		
		//Vollparametriesierte Rechteck
		Punkt p5 = new Punkt(200, 200);
		Rechteck r5 = new Rechteck(p5.getX(),p5.getY(),200,200);
		Punkt p6 = new Punkt(800, 400);
		Rechteck r6 = new Rechteck(p6.getX(),p6.getY(),20,20);
		Punkt p7 = new Punkt(800, 450);
		Rechteck r7 = new Rechteck(p7.getX(),p7.getY(),20,20);
		Punkt p8 = new Punkt(850, 400);
		Rechteck r8 = new Rechteck(p8.getX(),p8.getY(),20,20);
		Punkt p9 = new Punkt(855, 455);
		Rechteck r9 = new Rechteck(p9.getX(),p9.getY(),25,25);
		
		//Konsolen Ausgabe
		System.out.println(r0.toString().equals( "Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		//Controller
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(r0);
		controller.add(r1);
		controller.add(r2);
		controller.add(r3);
		controller.add(r4);
		controller.add(r5);
		controller.add(r6);
		controller.add(r7);
		controller.add(r8);
		controller.add(r9);
		
		//Controller Konsolen Ausgabe
		System.out.println(controller.toString());
		System.out.println(controller.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"));
		
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setPunkt(new Punkt(-10, -10));
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
		
		Rechteck randomrechteck1 = new Rechteck();
		randomrechteck1 = Rechteck.generiereZufallsRechteck();
		System.out.println("Zufall generiertes Rechteck" + randomrechteck1);
		
		System.out.println(rechteckeTesten());
	}
	
	public static boolean rechteckeTesten() {
		boolean aussage = false;
		Rechteck grundrechteck = new Rechteck (0, 0, 1200, 1000);
		Rechteck[] rechteckArray = new Rechteck[50000];
		for (int i = 0; i < rechteckArray.length; i++) {
			rechteckArray[i] = new Rechteck();
			rechteckArray[i]= Rechteck.generiereZufallsRechteck();
			if (grundrechteck.enthaelt(rechteckArray[i])) {
				System.out.println(i);
				aussage = true;
			}
			if (!(grundrechteck.enthaelt(rechteckArray[i]))) 
				return false;
			
		}
		System.out.println(rechteckArray.length);
		return aussage;
			
		}
 	}
	


