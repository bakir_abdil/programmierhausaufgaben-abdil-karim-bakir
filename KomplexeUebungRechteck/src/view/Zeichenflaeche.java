package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche  extends JPanel {

	//Atribute
	private final BunteRechteckeController controller;

	//Konstruktor 
	public Zeichenflaeche(BunteRechteckeController brc) {
		controller = brc;
	}
	
	//Methoden
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);

		for (int i = 0; i < controller.getRechtecke().size(); i++) {
			g.drawRect(controller.getRechtecke().get(i).getPunkt().getX(), controller.getRechtecke().get(i).getPunkt().getY(),
					controller.getRechtecke().get(i).getBreite(), controller.getRechtecke().get(i).getHoehe());
		}
	}
}
