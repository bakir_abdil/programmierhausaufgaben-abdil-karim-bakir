package model;

import java.util.Random;

public class Rechteck {

	// Attribute
	private Punkt p = new Punkt();
	private int breite;
	private int hoehe;

	// Konstruktor
	public Rechteck() {
		this.p.setX(0);
		this.p.setY(0);;
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.p.setX(x);
		this.p.setY(y);;
		this.breite = checkValue(breite);
		this.hoehe = checkValue(hoehe);
	}

	// Methoden
	public void setPunkt(Punkt p) {
		this.p = p;
	}

	public Punkt getPunkt() {
		return this.p;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = checkValue(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = checkValue(hoehe);
	}
	
	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
	public int checkValue(int zahl) {
		return Math.abs(zahl);
	}
	
	public boolean enthaelt(int x, int y) {
		return imRechteck(x,y);
	}
	
	public boolean enthaelt(Punkt p) {
		return imRechteck(p.getX(), p.getY());
	}
	
	public boolean imRechteck(int x, int y) {
		return breite > 0 && hoehe > 0
					&& x >= this.p.getX() && x < this.p.getX() + breite
					&& y >= this.p.getY() && y < this.p.getY() + hoehe;
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		return breite > 0 && hoehe > 0 && rechteck.getBreite() > 0 && rechteck.getHoehe() > 0
		&& rechteck.getPunkt().getX() >= this.p.getX() && rechteck.getPunkt().getX() + rechteck.getBreite() <= this.p.getX() + this.breite
		&& rechteck.getPunkt().getY() >= this.p.getY() && rechteck.getPunkt().getY() + rechteck.getHoehe() <= this.p.getY() + this.hoehe;
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random rgen = new Random();
		int x = rgen.nextInt(1200);
		int y = rgen.nextInt(1000);
		int nbreite = rgen.nextInt(x);
		int nhoehe = rgen.nextInt(y);
		return new Rechteck(x, y, nbreite, nhoehe);
	}
	
}
