package controller;

import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {

	// Attribute
	private LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();

	// leere Main-Methode
	public static void main(String[] args) {
	}

	// Konstruktor
	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}
	
	//Methoden
	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}
	
	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

}
